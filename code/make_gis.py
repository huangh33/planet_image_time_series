import os, imageio, fnmatch


imagedir='/mnt/research/sat.6/desert_large_votex/jpeg/'

tmpfiles=os.listdir(imagedir)

files=[]


for ifile in tmpfiles:
    if fnmatch.fnmatch(ifile, '*.jpeg'):
        ifile=os.path.join(imagedir, ifile)
        files.append(ifile)

outputfile='test.gif'

images=[]

for filename in files:
    images.append(imageio.imread(filename))

imageio.mimsave('movie.gif', images) 

