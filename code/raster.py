import numpy as np
import time
import os, fnmatch, importlib
import rasterio
from rasterio.transform import Affine
from rasterio.plot import show
from rasterio.merge import merge

import scipy.misc

import functions
importlib.reload(functions)

from  functions import write_jpeg
from  functions import strech
from  functions import extract_date
from  functions import find_uniq
from  functions import generate_jpeg

def check_samecrs(crs=[]):

    values=[]

    for i in crs:
        values.append(['init'])

    return(values)


datadir='/mnt/research/sat.6/desert_large_votex/tiffiles/'
outpurdir='/mnt/research/sat.6/desert_large_votex/jpeg/'

allfiles=os.listdir(datadir)

tiffiles=[]

for i in allfiles:
    if fnmatch.fnmatch(i, '*SR.tif'):
        i=os.path.join(datadir, i)
        tiffiles.append(i)


left=[]
right=[]
top=[]
bottom=[]
crs=[]

jpegflag=1

res=3

bandname='nir'

if bandname == 'nir':
    bandindex=4

if bandname == 'red':
    bandindex=3


#tiffiles=tiffiles[0:8]

single_band_files=[]

for ifile in tiffiles:   
    
    tmpfile=ifile

    #try:
    
    dataset=rasterio.open(tmpfile)
    
    data = dataset.read(bandindex)

    blank_dtype=data.dtype
    
    height=dataset.height
    
    width=dataset.width

    bounds=dataset.bounds

    left.append(bounds.left)
    
    right.append(bounds.right)
    
    top.append(bounds.top)
    
    bottom.append(bounds.bottom)
    
    crs.append(dataset.crs)
        
    geoinfo={'dtype':data.dtype, 'crs':dataset.crs, 'height':dataset.height, 'width':dataset.width, 'transform':dataset.transform}
        
    tmpbandfile=write_jpeg(filename=tmpfile, data=data, geoinfo=geoinfo, outpurdir=outpurdir)

    single_band_files.append(tmpbandfile)




crsvalues=np.array(check_samecrs(crs=crs))

if len(np.unique(crsvalues)) != 1:
    print('cross utm zone')
    time.sleep(1000)


top=max(top)
left=min(left)
right=max(right)
bottom=min(bottom)

xx=range(int(left), int(right)+res, res)

yy=range(int(top), int(bottom)-res, -1*res)

nx=len(xx)

ny=len(yy)

blank_arrray=np.zeros([nx, ny], dtype=blank_dtype)

blank_bounds=rasterio.coords.BoundingBox(left=left, bottom=bottom, right=right, top=top)

blank_width=nx

blank_height=ny

blank_crs=dataset.crs

blank_transform=Affine.translation(left, top)*Affine.scale(res, -res)

geoinfo={'dtype':blank_dtype, 'crs':blank_crs, 'height':blank_height, 'width':blank_width, 'transform':blank_transform}

dumpoutputfile='/mnt/research/sat.6/desert_large_votex/dump.tif'
        
new_dataset = rasterio.open(dumpoutputfile, "w", driver='GTiff', count=1, dtype=blank_dtype, crs=blank_crs, height=blank_height, width=blank_width,transform=blank_transform)
       
new_dataset.write(blank_arrray, 1)
       
new_dataset.close()


dates=extract_date(filenames=single_band_files)

uniquedate=find_uniq(lists=dates)

maxvalue=3000
minvalue=100
fillvalue=0
logflag=0

for iuniq in uniquedate:
    
    print(iuniq)

    tmpdatefile=[]

    daterange=range(0, len(dates))
    
    for idate in daterange:
        
        if dates[idate] == iuniq:

            tmpdatefile.append(single_band_files[idate])

            generate_jpeg(filenames=tmpdatefile,dumpfile=dumpoutputfile, outputdir=outpurdir, date=iuniq, maxvalue=maxvalue, minvalue=minvalue, fillvalue=fillvalue, logflag=logflag, geoinfo=geoinfo, bandname=bandname)










#
