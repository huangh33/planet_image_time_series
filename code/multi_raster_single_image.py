import numpy as np
import time
import os, fnmatch, importlib
import rasterio
from rasterio.transform import Affine
from rasterio.plot import show
from rasterio.merge import merge

import scipy.misc

import functions
importlib.reload(functions)

from  functions import write_jpeg
from  functions import strech
from  functions import extract_date
from  functions import find_uniq
from  functions import generate_jpeg

def check_samecrs(crs=[]):

    values=[]

    for i in crs:
        values.append(['init'])

    return(values)


truecolor=1

greencolorflag=0


fillvalue=0; logflag=1


nirrange   =[800,4000]
redrange   =[100,2000]
greenrange =[100,2000]
bluerange  =[100,1500]




nirrange   =[800,4000]
#
redrange   =[np.exp(5.400),np.exp(7.4)]
greenrange =[np.exp(5.400),np.exp(7.4)]
bluerange  =[np.exp(5.400),np.exp(7.4)]



colorrange=[bluerange, greenrange, redrange, nirrange]




datadir='/mnt/research/sat.6/desert_large_votex/tiffiles/'
outpurdir='/mnt/research/sat.6/desert_large_votex/jpeg/'

allfiles=os.listdir(datadir)

tiffiles=[]

for i in allfiles:
    if fnmatch.fnmatch(i, '*SR.tif'):
        i=os.path.join(datadir, i)
        tiffiles.append(i)



left=[];right=[];top=[]; bottom=[]; crs=[]

res=3

datasets=[]


readonefile=0

for ifile in tiffiles:   
    
    tmpfile=ifile

    dataset=rasterio.open(tmpfile)

    
    if readonefile==0:
        data=dataset.read()
        nn=data.shape
        nbands=nn[0]
        readonefile==1


    height=dataset.height
    
    width=dataset.width

    bounds=dataset.bounds

    left.append(bounds.left)
    
    right.append(bounds.right)

    crs.append(dataset.crs)

    top.append(bounds.top)
    
    bottom.append(bounds.bottom)


crsvalues=np.array(check_samecrs(crs=crs))

if len(np.unique(crsvalues)) != 1:
    print('cross utm zone')
    time.sleep(1000)



top=max(top);left=min(left) ;right=max(right);bottom=min(bottom)

xx=range(int(left), int(right)+res, res)

yy=range(int(top), int(bottom)-res, -1*res)

nx=len(xx); ny=len(yy)

blank_arrray=np.zeros([nx, ny], dtype=data.dtype)

blank_bounds=rasterio.coords.BoundingBox(left=left, bottom=bottom, right=right, top=top)

blank_width=nx; blank_height=ny; blank_crs=dataset.crs

blank_transform=Affine.translation(left, top)*Affine.scale(res, -res)

geoinfo={'dtype':data.dtype, 'crs':blank_crs, 'height':blank_height, 'width':blank_width, 'transform':blank_transform}


dumpoutputfile='/mnt/research/sat.6/desert_large_votex/dump_multi_bands.tif'
print('done')

new_dataset = rasterio.open(dumpoutputfile, "w", driver='GTiff', count=nbands, dtype=data.dtype, crs=blank_crs, height=blank_height, width=blank_width,transform=blank_transform)

for i in [1,2,3,4]:
    new_dataset.write(blank_arrray, i)

new_dataset.close()


dump=rasterio.open(dumpoutputfile)

dates=extract_date(filenames=tiffiles)

uniquedate=find_uniq(lists=dates)


for ifile in tiffiles:

    src_files_to_mosaic = []
    
    src=rasterio.open(ifile)

    tmpvar=ifile.split('/')
    tmpvar=tmpvar[len(tmpvar)-1]
    
    tmpvar=tmpvar.split('_')

    tiffileroot=tmpvar[0]+'_'+tmpvar[1]+'_'+tmpvar[2]

    src_files_to_mosaic.append(src)

    src_files_to_mosaic.append(dump)

    mosaicvalue, out_trans = merge(src_files_to_mosaic, method="max")


    if greencolorflag==0:
        if truecolor==1:
            red  =mosaicvalue[2,:,:]
            green=mosaicvalue[1,:,:]
            blue =mosaicvalue[0,:,:]

            redrange  =colorrange[2]
            greenrange=colorrange[1]
            bluerange =colorrange[0]
    
        else:
            red  =mosaicvalue[3,:,:]
            green=mosaicvalue[2,:,:]
            blue =mosaicvalue[1,:,:]

            redrange  =colorrange[3]
            greenrange=colorrange[2]
            bluerange =colorrange[1]


    else:
        red  =mosaicvalue[2,:,:]
        green=mosaicvalue[3,:,:]
        blue =mosaicvalue[1,:,:]

        redrange  =colorrange[2]
        greenrange=colorrange[3]
        bluerange =colorrange[1]


    
    red=strech(input_data=red, maxvalue=redrange[1], minvalue=redrange[0], fillvalue=fillvalue, logflag=logflag)

    green=strech(input_data=green, maxvalue=greenrange[1], minvalue=greenrange[0], fillvalue=fillvalue, logflag=logflag)

    blue=strech(input_data=blue, maxvalue=bluerange[1], minvalue=bluerange[0], fillvalue=fillvalue, logflag=logflag)

    nrg = np.dstack((red, green, blue))

    outputfile=outpurdir+tiffileroot+'.jpeg'

    scipy.misc.imsave(outputfile, nrg)







