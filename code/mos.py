import numpy as np
import time
import os, fnmatch, importlib
import rasterio
from rasterio.transform import Affine
from rasterio.plot import show
from rasterio.merge import merge

import scipy.misc

import functions
importlib.reload(functions)

from  functions import write_jpeg

def check_samecrs(crs=[]):

    values=[]

    for i in crs:
        values.append(i['init'])

    return(values)


datadir='/mnt/research/sat.6/desert_large_votex/tiffiles/'
outpurdir='/mnt/research/sat.6/desert_large_votex/jpeg/'

allfiles=os.listdir(datadir)

tiffiles=[]

for i in allfiles:
    if fnmatch.fnmatch(i, '*SR.tif'):
        i=os.path.join(datadir, i)
        tiffiles.append(i)

tiffiles=tiffiles[0]

for ifile in tiffiles:   
    
    tmpfile=ifile

    #try:
    
    dataset=rasterio.open(tmpfile)
    
    data = dataset.read(4)

