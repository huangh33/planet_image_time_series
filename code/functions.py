import os, fnmatch
import rasterio
import scipy.misc
import numpy as np
import rasterio
from rasterio.transform import Affine
from rasterio.plot import show
from rasterio.merge import merge


'''the functions'''


def write_jpeg(filename='', data=0, geoinfo=0, outpurdir=''):

    """ output the np.array data to jpeg file

    Keyword arguments:
        filename: the jpeg filename
        data: the output data, type == np.array
        geoinfo: the geo information, dict{}
        outputdir: the output dir
    """

    tmpfile=filename
    
    tmpvar=tmpfile.split('/')
       
    tmpvar=tmpvar[len(tmpvar)-1]

    outputfile=os.path.join(outpurdir, tmpvar)

    #scipy.misc.imsave(outputfile, data)

    dtype=geoinfo['dtype']
    crs=geoinfo['crs']
    height=geoinfo['height']
    width=geoinfo['width']
    transform=geoinfo['transform']

    outputfile=outputfile+'.tif'
       
    new_dataset = rasterio.open(outputfile, "w", driver='GTiff', count=1, dtype=dtype, crs=crs, height=height, width=width, transform=transform)
       
    new_dataset.write(data, 1)
       
    new_dataset.close()

    return(outputfile)



def strech(input_data=0, maxvalue=0, minvalue=0, fillvalue=0, logflag=0):
    """
        strech input data to the range of 0-255
        
        Keyword arguments:
            input_data:  input data, type = np.array, read from tif file
            maxvalue: the upper limit of the input_data valid values
            minvalue: the lower limit of the input_data valid values.
            fillvalue: the value of bad value
            logflag:  1: peform log stretch
                      0: perform linear stretch
    """

    input_data=input_data.astype('float32')

    print('input type')
    print(input_data.dtype)

    nn=input_data.shape

    result=np.zeros([nn[0], nn[1]], dtype='uint8')

    fill_index=np.where(input_data == fillvalue)

    if(len(fill_index)>0): result[fill_index]=0

    if logflag==1:
        maxvalue=np.log(maxvalue)
        minvalue=np.log(minvalue)
    
        nozero_index=np.where(input_data != 0)
        
        if(len(nozero_index)>0): 
            input_data[nozero_index]=np.log(input_data[nozero_index])
#             tmp=np.log(input_data[nozero_index])
#             print('tmp.dtype')
#             print(tmp.dtype)




    
    max_index=np.where(input_data >= maxvalue)

    if(len(max_index)>0): result[max_index]=255

    min_index=np.where(input_data<=minvalue)

    if(len(min_index)>0): result[min_index]=0

    values_index=np.where((input_data > minvalue) & (input_data < maxvalue))
   
    if(len(values_index)>0):

#        result[values_index]=(255.0*(input_data[values_index]-minvalue)/float(maxvalue-minvalue))

        tmp=(255.0*(input_data[values_index]-minvalue)/float(maxvalue-minvalue))

    

        result[values_index]=tmp.astype('uint8')

    return(result)



    

def extract_date(filenames=[]):
    """
    extract yyymmdd information from filenames

    Keyword arguments: 
        filenames: a list of string
    return:
        dates in format of yyyymmdd, list of str
    """

    dates=[]

    for ifile in filenames:
        
        tmpvar=ifile.split('/')
        
        tmpvar=tmpvar[len(tmpvar)-1]
        
        tmpvar=tmpvar.split('_')

        dates.append(tmpvar[0])

    return(dates)

        
def find_uniq(lists=[]):
    
    unique_list=[]
    for x in lists:
        if x not in unique_list:
            unique_list.append(x)

    return(unique_list)


def find_index(lists=[], matchvalue=''):

    result=[]

    nlen=range(0, len(lists))

    for i in nlen:
        
        if lists[i] == matchvalue:
            result.append(i)

    return(result)
   


def generate_jpeg(filenames=[],dumpfile=[], outputdir='', date='', maxvalue=0, minvalue=0, fillvalue=0, logflag=0, geoinfo=0, bandname=''):
    
    single_band_files=filenames

    dumpoutputfile=dumpfile

    src_files_to_mosaic = []

    for fp in single_band_files:
    
        print(fp)
        
        src=rasterio.open(fp)
        
        src_files_to_mosaic.append(src)

    
    dump=rasterio.open(dumpoutputfile)
        
    src_files_to_mosaic.append(dump)
    
    mosaicvalue, out_trans = merge(src_files_to_mosaic, method="max")
    
    outputfile='tmp.tif'
   
    tmpfile=outputfile

    blank_dtype=geoinfo['dtype']
    
    blank_crs=geoinfo['crs']
    
    blank_height=geoinfo['height']

    blank_width=geoinfo['width']

    blank_transform=geoinfo['transform']
    
    new_dataset = rasterio.open(outputfile, "w", driver='GTiff', count=1, dtype=blank_dtype, crs=blank_crs, height=blank_height, width=blank_width,transform=blank_transform)
           
    new_dataset.write(mosaicvalue)
           
    new_dataset.close()
    
    mos_data=rasterio.open(outputfile)
    tmpmos_data = mos_data.read(1)
    
   
    tmpmos_data=strech(input_data=tmpmos_data, maxvalue=maxvalue, minvalue=minvalue, fillvalue=fillvalue, logflag=logflag)
    
    outputfile=outputdir+bandname+'_'+date+'.jpeg'
    
    scipy.misc.imsave(outputfile, tmpmos_data)
    
    os.remove(tmpfile)


